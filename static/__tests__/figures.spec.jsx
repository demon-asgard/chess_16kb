import React from 'react';
import renderer from 'react-test-renderer'
import Enzyme, { shallow, mount, render } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
Enzyme.configure({ adapter: new Adapter() });


import {Board, Figure} from "../board";


it('should render a figure', () => {
    const wq = render(
        <Figure type="q" color="w" />
    );
    expect(wq).toMatchSnapshot();

    const bk = render(
        <Figure type="k" color="black" />
    );
    expect(bk).toMatchSnapshot();
});

it('should render a board', () => {
    const fen = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1";

    const b = render(
        <Board fen={fen} />
    );
    expect(b).toMatchSnapshot();
});
